#!/bin/env python3

import re


roomstack = []
bytesout = 0


def asyescape(line):
    return (line.replace("|", "/")
            .replace(" #", " number ")
            .replace("#", "hash"))


def countbytes(line):
    global bytesout
    print(line)
    bytesout += len(line)


def spaceout(space):
    global bytesout

    if(bytesout > 20000):
        print("# --------SPLIT-----------")
        for i in range(0, len(roomstack)):
            if i == 0:
                parent = ""
            else:
                parent = roomstack[i-1][0]
            countbytes(f"{asyescape(roomstack[i][0])}||{asyescape(parent)}")
        bytesout = 0
    roomout(space)


def roomout(room):
    countbytes(f"{asyescape(room)}||{asyescape(roomstack[-1][0])}")


def process(line):
    global possiblespace

    if(line == 'Join'):
        pass
    elif(re.match(r'.*[0-9]+ rooms?', line)):
        # the last possible space we saw is a space
        # it will be parent of all rooms to come

        # This is horrendous. A space with no rooms but only subspaces reports
        # 0 rooms which means we can never know when it ends. Here we hardcode
        # the boundaries of the end of the Main Tracks (Devrooms) and Devroom
        # spaces (Stands) so we know to pop again
        if(possiblespace == "Devrooms" or
           possiblespace == "Stands"):
            roomstack.pop()

        possiblespace = possiblespace + " space"
        spaceout(possiblespace)
        rooms = int(re.findall(r'([0-9]+) rooms?', line)[0])
        roomstack.append((f"{possiblespace}", rooms))
    elif(re.match(r'^[0-9]+ members', line)):
        # last one was a room
        # decrement the room counter on current room stack
        # if zero pop it off
        roomout(f"{possiblespace}")

        (space, roomsleft) = roomstack.pop()
        roomsleft -= 1
        if(roomsleft > 0):
            roomstack.append((space, roomsleft))
    else:
        possiblespace = line


with open('FOSDEM.txt') as f:
    roomstack.append(("FOSDEM 2022", 999))
    for line in f:
        process(line.strip())
